package mojaCiesla.p5;//Piotr Szegda
//Zadanie 2
import static java.lang.Math.abs;

interface Function{
    double getValue(double x);
}

class Test
        implements Function {
    @Override
    public double getValue(double x) {
        return ((-2.0*x)+4.0);
    }
}

public class Main {

    public static void main(String[] args) {
        if (args.length !=3)
        {
            System.out.println("Niepoprawna ilość argumentów");
            return;
        }

        Function f;
        try {
            f = (Function) Class.forName(args[0]).newInstance();
        }
        catch (Exception e){
            System.out.println("Problem z utworzeniem klasy.");
            return;
        }
        Bisection bisection = new Bisection();
        double result = bisection.find( Double.parseDouble(args[1]), Double.parseDouble(args[2]), f);
        System.out.println("Wynik:");
        System.out.println(result);

    }

}

class Bisection{
    public static double find( double a, double b, Function f){
        double epsilon = 0.0000000000000000001;
        double residuum = 0.000000000000000001;
        double x1;

        while (abs(a - b) > epsilon) {
            x1 = (a + b) / 2;
            if (abs(f.getValue(x1)) <= residuum) break;
            else if (f.getValue(x1) * f.getValue(a) < 0)
            b = x1;
            else
            a = x1;
        }

        return (a + b) / 2;

    }

}

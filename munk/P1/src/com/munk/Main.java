package com.munk;

import com.munk.wyklad6.SimplePrime;
import com.munk.wyklad6.SimplePrimeAsync;
import javafx.util.Pair;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    public static void main(String[] args) {

//        //Wykład 4
//        var pq = new OwnPriorityQueue<String>();
//        var w = "ww";
//        pq.add("123", 1);
//        pq.add("test", 0);
//        pq.add("test", 4343);
//        while (pq.size() > 0)
//        {
//            System.out.println(pq.peek());
//            pq.get();
//        }
//
////        //Wykład 5 -1
//        var fileP = new FileP(FileSystems.getDefault().getPath("pan-tadeusz.txt"));
//        fileP.start();
//
//        //Wykład 5 -2
//        try {
//            Scanner sc = new Scanner(System.in);
//
//            PrintStream console = System.out;
//
//            var path = FileSystems.getDefault().getPath("test.txt");
//            var in = new BufferedReader(new FileReader(path.toString()));
//            var s = new Scanner(in);
//
//            var gos = new InputCesarCipher(console, 1);
//
//            while (s.hasNext()) {
//                var world = s.next();
//                gos.write(world.getBytes());
//            }
//            sc.close();
//            gos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //Wykład 6
        long standard_time = 0;
        {
            var start = System.nanoTime();
            List<Long> r = new LinkedList<>();
            for (long i = 1; i != 9000; i++) {
                var w = SimplePrime.calc(i);
                if (w.getKey()) r.add(w.getValue());
            }
            var end = System.nanoTime();
            standard_time = end-start;
            for (var w: r) {
                //      System.out.println(w);
            }
            System.out.println("Czas: " + standard_time);
        }
        long async_time = 0;
        try {
            var start = System.nanoTime();
            List<Future<Pair<Boolean, Long>>> l = new LinkedList<>();
            List<Long> r = new LinkedList<>();
            for (long i=1;i != 9000;i++) {
                    l.add(SimplePrimeAsync.calculateAsync(i));
            }

            for (var w: l) {
                if (w.get().getKey()) r.add(w.get().getValue());
            }
            var end = System.nanoTime();
            for (var w: r) {
           //     System.out.println(w);
            }
            async_time = end-start;
            System.out.println("Czas: " + async_time);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        //Wersja async działa wolniej pewnie przez natrzuty na tworzenie wątków które są większe niż czas obliczeń.
        //Może to inaczej zoptymalizować.




    }
}

package mojaCiesla.p4;

public class GameLogic {
    int size = 3;
    public TicTacMap ticTacMap;

    GameLogic(){
        ticTacMap = new TicTacMap();
    }

    Field checkResult(){

        for (int x = 0; x < size; ++x){
            for (int y = 1; y < size; ++y) {
                Field field = null;
                if (field == null) {
                    field = ticTacMap.Get(x, 0);
                    if (field == Field.Empty) break;
                }
                if (ticTacMap.Get(x, y) != field) break;
                if (y == size -1) return field;
            }
        }

        for (int y = 0; y < size; ++y){
            for (int x = 1; x < size; ++x) {
                Field field = null;
                if (field == null) {
                    field = ticTacMap.Get(0, y);
                    if (field == Field.Empty) break;
                }
                if (ticTacMap.Get(x, y) != field) break;
                if (x == size -1) return field;
            }
        }

        if (checkDiagonal(Field.Noughts)) return Field.Noughts;
        if (checkDiagonal(Field.Crosses)) return Field.Crosses;

        if (checkIsDraw()) return Field.Empty;


        return null;
    }

    Boolean checkDiagonal(Field field){
        if ( ticTacMap.Get(0, 0) == field && ticTacMap.Get(1, 1) == field && ticTacMap.Get(2, 2) == field) return true;
        if ( ticTacMap.Get(0, 2) == field && ticTacMap.Get(1, 1) == field && ticTacMap.Get(0, 2) == field) return true;
        return false;
    }
    Boolean checkIsDraw(){
        for (int x = 0; x < size; ++x){
            for (int y = 0; y < size; ++y) {
                if (ticTacMap.Get(x, y) == Field.Empty) return false;
            }
        }

        return true;
    }
    Field write(int x, int y, Field fieldStatus){
        if (ticTacMap.Get(x, y) != Field.Empty){
            //GameLogic Error
        }
        else ticTacMap.Set(x, y, fieldStatus);
        return checkResult();
    }


    Field Get(int x, int y){
        return ticTacMap.Get(x, y);
    }

}

package mojaCiesla.p3;

public interface TicTacNetworkInterface {
    void Select(int x, int y);
    void start();
    void stop();
    void attachUI(TicTacUI ui);
    int getSize();
    Field getField(int x, int y);
}

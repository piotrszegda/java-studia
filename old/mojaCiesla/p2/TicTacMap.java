package mojaCiesla.p2;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Vector;

enum Field{
    Empty, Noughts, Crosses
}

class elementOnMap implements java.io.Serializable  {
    @XmlElement(name = "field")
Field field;
    @XmlElement(name = "x")
int x;
    @XmlElement(name = "y")
int y;
}

@XmlRootElement
public class TicTacMap implements java.io.Serializable {


    private Vector<elementOnMap> TicTacMap = new Vector<>();


    @XmlElement(name = "q")
    public Vector<elementOnMap> getVector(){
        return TicTacMap;
    }


    Field Get(int x, int y){
        elementOnMap result = null;
        for (elementOnMap obj: TicTacMap) {
            if (obj.x == x && obj.y == y) {
                return obj.field;
            }
        }
        return Field.Empty;
    }

    void Set(int x, int y, Field field){
        elementOnMap result = null;
        for (elementOnMap obj: TicTacMap) {
            if (obj.x == x && obj.y == y) {
                TicTacMap.remove(obj);
            }
        }
        elementOnMap toAdd = new elementOnMap();
        toAdd.x = x;
        toAdd.y = y;
        toAdd.field = field;
        TicTacMap.add(toAdd);

    }

}

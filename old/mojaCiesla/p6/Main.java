package mojaCiesla.p6;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;

public class Main {

    public static void main(String[] args) throws IOException {
        final File path = new File(args[0]);
        if (args.length != 2) {
            System.out.println("Błędna ilość argumentów");
            return;
        }
        boolean encrypt;
        if (args[1] == "encrypt") encrypt = true;
        else encrypt = false;
        if (path.isDirectory()){
            if (encrypt) encryptAll(path);
            else decryptAll(path);
        }
        else {
            if (encrypt) encrypt(path.getAbsolutePath(), path.getAbsolutePath()+"_encrypted");
            else encrypt(path.getAbsolutePath(), path.getAbsolutePath()+"_encrypted");
        }
    }

    public static void encryptAll(final File folder) throws IOException {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory() == false) {
                encrypt(fileEntry.getAbsolutePath(), fileEntry.getAbsolutePath()+"_encrypted");
                fileEntry.delete();
            }
        }
    }

    public static void decryptAll(final File folder) throws IOException {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory() == false) {
                decrypt(fileEntry.getAbsolutePath(), fileEntry.getAbsolutePath()+"_decrypted");
                fileEntry.delete();
            }
        }
    }

    public static void decrypt (String input, String output) throws IOException{
        FileReader in = null;
        FileWriter out = null;
        try {
            in = new FileReader(input);
            out = new FileWriter(output);
            int c;
            int position = 0;
            while ((c = in.read()) != -1) {
                String tmp = String.valueOf(( (char)c) );
                c = c - 97;
                if (isAlpha(tmp)) {
                    System.out.println("Encoded: " + tmp);
                    c = c - position;
                    System.out.println("Decoded: " + (char) (c + 97));
                }
                out.write((char) (c) + 97);
                position++;
            }
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
        }
    }

    public static void encrypt(String input, String output) throws IOException{
        FileReader in = null;
        FileWriter out = null;
        try {
            in = new FileReader(input);
            out = new FileWriter(output);
            int c;
            int position = 0;
            while ((c = in.read()) != -1) {
                String tmp = String.valueOf(((char)c));
                tmp = tmp.toLowerCase();
                tmp = Normalizer.normalize(tmp, Normalizer.Form.NFD);
                c = tmp.toCharArray()[0];
                c = c - 97;
                if (isAlpha(tmp)) {
                    System.out.println("PlainText: " + tmp);
                    c = (c + position) % 26;
                }

                System.out.println("Encoded: "+ (char)(c+97));
                out.write((char)(c+97));
                position++;
            }
        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
        }
    }

    public static boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }





}

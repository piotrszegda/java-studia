package com.company;

import javafx.util.Pair;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class TicTacClient implements TicTacNetworkInterface {

    TicTacClient(InetAddress address, int port) throws IOException {
        socket = new Socket(address, port);
        outputStream = socket.getOutputStream();
        inputStream = socket.getInputStream();
        objectOutputStream = new ObjectOutputStream(outputStream);
        objectInputStream = new ObjectInputStream(inputStream);
    }

    @Override
    public void Select(int x, int y){
        try {
            ClientToServerRequest clientToServerRequest = new ClientToServerRequest();
            clientToServerRequest.clientToServerRequestType = ClientToServerRequestType.SendOwnMove;
            clientToServerRequest.move = new Pair(x,y);
           objectOutputStream.writeObject(clientToServerRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        working = true;
        thread = new Thread(() -> {
            while (working) {
                try {
                    ServerToClientRequest serverToClientRequest = (ServerToClientRequest) objectInputStream.readObject();
                    if (serverToClientRequest.serverToClientRequestType == ServerToClientRequestType.GameEnd) {
                        if (serverToClientRequest.whoWon == Field.Empty )ticTacUI.showMessage("Remis");
                        if (serverToClientRequest.whoWon  == Field.Crosses )ticTacUI.showMessage("Wygrały krzyżyki ");
                        if (serverToClientRequest.whoWon  == Field.Noughts )ticTacUI.showMessage("Wygrały kółka ");
                    }
                    if (serverToClientRequest.serverToClientRequestType == ServerToClientRequestType.UpdateMap){
                        ticTacMap = serverToClientRequest.ticTacMap;
                        ticTacUI.updateMap();
                        if (serverToClientRequest.blockPlay) ticTacUI.blockPlay();
                        else ticTacUI.readyForPlay();
                    }
                } catch (IOException e) {
                    working = false;
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    working = false;
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    @Override
    public void stop() {

    }

    @Override
    public void attachUI(TicTacUI ui) {
        ticTacUI = ui;
    }

    @Override
    public Field getField(int x, int y){
        return ticTacMap.Get(x,y);
    }

    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private TicTacUI ticTacUI;
    private Thread thread;
    private TicTacMap ticTacMap;
    private boolean working = false;
}

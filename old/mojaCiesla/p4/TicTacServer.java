package mojaCiesla.p4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TicTacServer implements TicTacNetworkInterface {

    TicTacServer(int port) throws IOException {
        serverSocket = new ServerSocket(Integer.valueOf(port));
        gameLogic = new GameLogic();
    }

    @Override
    public void Select(int x, int y) {
        gameLogic.write(x, y, Field.Crosses);
        SendMapUpdate(false);
        ticTacUI.blockPlay();
        ticTacUI.updateMap();


    }

    private void SendMapUpdate(boolean blockPlay){
        try {
            Field result = gameLogic.checkResult();
            if (result != null){
                ServerToClientRequest serverToClientRequest = new ServerToClientRequest();
                serverToClientRequest.serverToClientRequestType = ServerToClientRequestType.GameEnd;
                serverToClientRequest.whoWon = result;
                if (result == Field.Empty )ticTacUI.showMessage("Remis");
                if (result == Field.Crosses )ticTacUI.showMessage("Wygrały krzyżyki ");
                if (result == Field.Noughts )ticTacUI.showMessage("Wygrały kółka ");
                blockPlay = true;
                objectOutputStream.writeUnshared(serverToClientRequest);
                gameLogic = new GameLogic();
                ticTacUI.updateMap();
                ticTacUI.readyForPlay();
            }
            ServerToClientRequest serverToClientRequest = new ServerToClientRequest();
            serverToClientRequest.serverToClientRequestType = ServerToClientRequestType.UpdateMap;
            serverToClientRequest.ticTacMap = gameLogic.ticTacMap;
            serverToClientRequest.blockPlay = blockPlay;
            objectOutputStream.writeUnshared(serverToClientRequest);
            objectOutputStream.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        thread = new Thread(() -> {
            try {
                System.out.println("Server stared, waiting for connection.");
                socket = serverSocket.accept();
                System.out.println("Connected");
                ticTacUI.readyForPlay();
                outputStream = socket.getOutputStream();
                inputStream = socket.getInputStream();
                objectOutputStream = new ObjectOutputStream(outputStream);
                objectInputStream = new ObjectInputStream(inputStream);
                working = true;
                while (working) {
                    try {
                        ClientToServerRequest clientToServerRequest = (ClientToServerRequest) objectInputStream.readObject();
                        gameLogic.write(clientToServerRequest.move.getKey(), clientToServerRequest.move.getValue(), Field.Noughts);
                        SendMapUpdate(true);
                        ticTacUI.readyForPlay();
                    } catch (IOException e) {
                        working = false;
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        working = false;
                        e.printStackTrace();
                    }
                    ticTacUI.updateMap();
                }
            } catch (Exception e) {
                ticTacUI.showMessage(e.getLocalizedMessage());
            }
        });
        thread.start();

    }

    @Override
    public void stop() {
    }

    @Override
    public void attachUI(TicTacUI ui) {
        ticTacUI = ui;
    }

    @Override
    public Field getField(int x, int y){
        return gameLogic.Get(x, y);
    }

    private GameLogic gameLogic;
    private Socket socket;
    ServerSocket serverSocket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private ObjectOutputStream objectOutputStream;
    private Thread thread;
    private TicTacUI ticTacUI;
    private boolean working;
    private ObjectInputStream objectInputStream;
}

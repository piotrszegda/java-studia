package com.munk.wyklad5;

import org.jetbrains.annotations.NotNull;

import java.io.OutputStream;

public class OutputCesarCipher extends InputCesarCipher {
    /**
     * Creates an output stream filter built on top of the specified
     * underlying output stream.
     *
     * @param out    the underlying output stream to be assigned to
     *               the field {@code this.out} for later use, or
     *               <code>null</code> if this instance is to be
     *               created without an underlying stream.
     * @param cipher
     */
    public OutputCesarCipher(@NotNull OutputStream out, int cipher) {
        super(out, -cipher);
    }
}

package mojaCiesla.p2;

enum ServerToClientRequestType{
    UpdateMap, GameEnd
}

public class ServerToClientRequest implements java.io.Serializable {
    ServerToClientRequestType serverToClientRequestType;
    TicTacMap ticTacMap;
    Field whoWon;
    boolean blockPlay;
}

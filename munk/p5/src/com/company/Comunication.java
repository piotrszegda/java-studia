package com.company;

import java.io.*;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Comunication {
    private Socket socket;
    private OutputStream outputStreamClient;
    private InputStream inputStreamClient;
    private ObjectOutputStream objectOutputStreamClient;
    private ObjectInputStream objectInputStreamClient;
    private ObjectOutputStream objectOutputStreamServer;
    private ObjectInputStream objectInputStreamServer;
    private OutputStream outputStreamServer;
    private InputStream inputStreamServer;
    ServerSocket serverSocket;
    private Socket socketServerSide;
    MyUI ui;
    private Thread thread;

    Comunication(InetAddress address, int port, int portLocal, MyUI cui) throws IOException {
        serverSocket = new ServerSocket(portLocal);


        incomingMessageProcess();
        boolean conncted = false;
        while (!conncted) {
            try {
                socket = new Socket(address, port);
                conncted = true;
            }
            catch (ConnectException e){
                System.out.println("Nieudane połączenie");
            }
        }
        outputStreamClient = socket.getOutputStream();
        inputStreamClient = socket.getInputStream();

        objectOutputStreamClient = new ObjectOutputStream(outputStreamClient);
        objectInputStreamClient = new ObjectInputStream(inputStreamClient);

        ui = cui;
    }

    void SendMessage(String msg) throws IOException {
        Message w = new Message();
        w.messageType = MessageType.TextMessage;
        w.message = msg;
        objectOutputStreamClient.writeUnshared(w);
    }

    public void incomingMessageProcess() {
        thread = new Thread(() -> {
                try {
                    Socket l_socket = serverSocket.accept();
                    outputStreamServer = l_socket.getOutputStream();
                    inputStreamServer = l_socket.getInputStream();
                    objectOutputStreamServer = new ObjectOutputStream(outputStreamServer);
                    objectInputStreamServer = new ObjectInputStream(inputStreamServer);
                    System.out.println("Connected");
                    while (true) {
                        Message message = (Message) objectInputStreamServer.readObject();
                        if (message.messageType == MessageType.TextMessage) ui.display(message.message);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

        });
        thread.start();

    }

    public void sendFile(Path path) throws IOException {
        Message w = new Message();
        w.messageType = MessageType.File;
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines( path, StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        w.message = contentBuilder.toString();
        objectOutputStreamClient.writeUnshared(w);
    }


}

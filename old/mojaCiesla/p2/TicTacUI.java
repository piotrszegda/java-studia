package mojaCiesla.p2;

public interface TicTacUI {
    void updateMap();
    void readyForPlay();
    void blockPlay();
    void showMessage(String error);
}

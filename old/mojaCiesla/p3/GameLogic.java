package mojaCiesla.p3;

public class GameLogic {
    int size = 20;
    int toWin = 5;
    public TicTacMap ticTacMap;
    private GameHistory gameHistory;
    GameLogic(){
        ticTacMap = new TicTacMap();
        gameHistory = new GameHistory();
        gameHistory.newGameHistory();
    }

    Field checkResult(int x, int y){
        Field last = ticTacMap.Get(x, y);
        if (ticTacMap.Get(x, y) != Field.Empty) {
            int match = 1;

            //up
            for (int i = 1; i < toWin; ++i) {
                int yToCheck = y - i;
                if (yToCheck < 0) break;
                if (ticTacMap.Get(x, yToCheck) != last) break;
                else match++;
            }

            //down
            for (int i = 1; i < toWin; ++i) {
                int yToCheck = y + i;
                if (yToCheck > size) break;
                if (ticTacMap.Get(x, yToCheck) != last) break;
                else match++;
            }

            if (match >= toWin) return last;
            match = 1;

            //left
            for (int i = 1; i < toWin; ++i) {
                int xToCheck = x - i;
                if (xToCheck < 0) break;
                if (ticTacMap.Get(xToCheck, y) != last) break;
                else match++;
            }

            //right
            for (int i = 1; i < toWin; ++i) {
                int xToCheck = x + i;
                if (xToCheck > size) break;
                if (ticTacMap.Get(xToCheck, y) != last) break;
                else match++;
            }

            if (match >= toWin) return last;
            match = 1;
            //ckeck diag up
            for (int i = 1; i < toWin; ++i) {
                int yToCheck = y - i;
                int xToCheck = x - i;
                if (yToCheck < 0 | xToCheck < 0) break;
                if (ticTacMap.Get(xToCheck, yToCheck) != last) break;
                else match++;
            }

            //ckeck diag down
            for (int i = 1; i < toWin; ++i) {
                int yToCheck = y + i;
                int xToCheck = x + i;
                if (yToCheck > size | xToCheck > size) break;
                if (ticTacMap.Get(xToCheck, yToCheck) != last) break;
                else match++;
            }

            if (match >= toWin) return last;
            match = 1;

            //ckeck antidiag up
            for (int i = 1; i < toWin; ++i) {
                int yToCheck = y + i;
                int xToCheck = x - i;
                if (yToCheck < 0 | xToCheck < 0) break;
                if (ticTacMap.Get(xToCheck, yToCheck) != last) break;
                else match++;
            }

            //ckeck antidiag down
            for (int i = 1; i < toWin; ++i) {
                int yToCheck = y - i;
                int xToCheck = x + i;
                if (yToCheck > size | xToCheck > size) break;
                if (ticTacMap.Get(xToCheck, yToCheck) != last) break;
                else match++;
            }

            if (match >= toWin) return last;
        }

        if (checkIsDraw()) return Field.Empty;


        return null;
    }

    Boolean checkIsDraw(){
        for (int x = 0; x < size; ++x){
            for (int y = 0; y < size; ++y) {
                if (ticTacMap.Get(x, y) == Field.Empty) return false;
            }
        }

        return true;
    }
    Field write(int x, int y, Field fieldStatus){
        if (ticTacMap.Get(x, y) != Field.Empty){
            //GameLogic Error
        }
        else {
            ticTacMap.Set(x, y, fieldStatus);
            gameHistory.addEntry(x,y, fieldStatus);
        }
        return checkResult(x,y);
    }


    Field Get(int x, int y){
        return ticTacMap.Get(x, y);
    }

}

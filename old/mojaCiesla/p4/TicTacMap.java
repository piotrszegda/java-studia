package mojaCiesla.p4;

import java.util.HashMap;
import java.util.Map;

enum Field{
    Empty, Noughts, Crosses
}

public class TicTacMap implements java.io.Serializable {
    private Map<Integer, Map<Integer, Field>> TicTacmap = new HashMap<>();
    Field Get(int x, int y){
        if (TicTacmap.containsKey(x) == false || TicTacmap.get(x).containsKey(y) == false) return Field.Empty;
        else return TicTacmap.get(x).get(y);
    }

    void Set(int x, int y, Field field){
        TicTacmap.putIfAbsent(x, new HashMap<Integer, Field>());
        TicTacmap.get(x).put(y, field);
    }

}

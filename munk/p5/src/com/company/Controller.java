package com.company;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;

public class Controller implements MyUI {
    public TextArea chatLog;
    public TextField ipField;
    public TextField textb;
    private Comunication comunication;

    @Override
    public void display(String msg) {
        Platform.runLater(() -> {
            chatLog.appendText(msg);
        });
    }

    public void StartConnection(ActionEvent actionEvent) throws IOException {
        chatLog.appendText("Próba połączenia \n");
        //TUTAJ MOŻNA ZMIENIC PORTY
        comunication = new Comunication(Inet4Address.getByName(ipField.getText()), 9876, 9876, this);

    }

    public void handleKeyInput(KeyEvent keyEvent) {
        Platform.runLater(() -> {
            chatLog.appendText(keyEvent.getCharacter());
            try {
                if (keyEvent.getCharacter() == "\r") textb.clear();
                comunication.SendMessage(String.valueOf(keyEvent.getCharacter()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

package com.munk.wyklad5;

import org.jetbrains.annotations.NotNull;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class InputCesarCipher extends FilterOutputStream {
    private int cipher;
    /**
     * Creates an output stream filter built on top of the specified
     * underlying output stream.
     *
     * @param out the underlying output stream to be assigned to
     *            the field {@code this.out} for later use, or
     *            <code>null</code> if this instance is to be
     *            created without an underlying stream.
     */
    public InputCesarCipher(@NotNull OutputStream out, int cipher) {
        super(out);
        this.cipher = cipher;
    }

    @Override
    public void write(@NotNull byte[] b) throws IOException {
        var s = new String(b);
        var r = new StringBuilder();
        for (char c: s.toCharArray()) {
            r.append((char)(c + cipher));

        }
        super.write(r.toString().getBytes());
    }
}

package com.company;

public interface TicTacNetworkInterface {
    void Select(int x, int y);
    void start();
    void stop();
    void attachUI(TicTacUI ui);
    Field getField(int x, int y);
}

package mojaCiesla.p7;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s;
        Map<Integer, Integer> SentenceLength =  new HashMap<>();
        int TotalS = 0;
        try {
            s = new Scanner(new BufferedReader(new FileReader("input.txt")));
            s.useDelimiter("\\.");

            while (s.hasNext()) {
                String currentSentence = s.next();
                int oc = 0;
                int words = currentSentence.trim().split("\\s+").length;
                if (currentSentence.trim().isEmpty()) continue;
                if (SentenceLength.containsKey(words)) {
                    oc = SentenceLength.get(words);
                }
                SentenceLength.put(words, ++oc);
                ++TotalS;
//                System.out.print(currentSentence);
//                System.out.print("!!ZDANIE!!");
//                System.out.println(words);
            }
        } catch (FileNotFoundException e) {
            System.out.print("Nie można otworzyć pliku.");
            e.printStackTrace();
        }

        double finalTotalS = TotalS;
        SentenceLength.forEach((k, v) -> System.out.println("Długość: "+k+" Wystąpień:"+v + " (" + (v/ (finalTotalS))*100 + "%)"));
    }
}

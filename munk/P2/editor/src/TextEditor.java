import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TextEditor extends JFrame {

    ActionMap actionMap;

    Action CutAction;

    Action CopyAction;

    Action PasteAction;

    Action NewAction;

    Action OpenAction;

    Action SaveAction;

    Action SaveAsAction;

    Action QuitAction;


    public TextEditor() {
        initAction();
        initUI();
    }

    private JFileChooser jFileChooser = new JFileChooser();
    private boolean anyChanges = false;
    private Path path = null;

    private JTextArea jTextArea = new JTextArea(40, 80);

    private void initAction() {
        actionMap = jTextArea.getActionMap();

        SaveAction = new AbstractAction("Save") {
            public void actionPerformed(ActionEvent e) {
                if (path != null) {
                    path = Paths.get(jFileChooser.getSelectedFile().getAbsolutePath());
                    writeFile(path);
                } else saveFileAs();
            }
        };

        OpenAction = new AbstractAction("Open") {
            public void actionPerformed(ActionEvent e) {
                saveCurrent();
                if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    path = Paths.get(jFileChooser.getSelectedFile().getAbsolutePath());
                    readFile(path);
                }
                SaveAsAction.setEnabled(true);
            }
        };

        NewAction = new AbstractAction("New") {
            public void actionPerformed(ActionEvent e) {
                saveCurrent();
                jTextArea.setText("");
                setTitle("Untitled.txt");
                anyChanges = false;
                path = null;
                SaveAction.setEnabled(false);
                SaveAsAction.setEnabled(false);
            }
        };

        QuitAction = new AbstractAction("Quit") {
            public void actionPerformed(ActionEvent e) {
                saveCurrent();
                System.exit(0);
            }
        };

        SaveAsAction = new AbstractAction("Save as...") {
            public void actionPerformed(ActionEvent e) {
                saveFileAs();
            }
        };

        PasteAction = actionMap.get(DefaultEditorKit.pasteAction);

        CopyAction = actionMap.get(DefaultEditorKit.copyAction);

        CutAction = actionMap.get(DefaultEditorKit.cutAction);

    }

    private void initUI(){

        JScrollPane jScrollPane = new JScrollPane(jTextArea);
        add(jScrollPane,BorderLayout.CENTER);

        JMenuBar jMenuBar = new JMenuBar();
        setJMenuBar(jMenuBar);
        JMenu fileMenu = new JMenu("File");

        fileMenu.add(NewAction);
        fileMenu.add(OpenAction);
        fileMenu.add(SaveAction);
        fileMenu.add(SaveAsAction);
        fileMenu.addSeparator();
        fileMenu.add(QuitAction);
        jMenuBar.add(fileMenu);

        JMenu editMenu = new JMenu("Edit");
        editMenu.add(CutAction).setName("Cut");
        editMenu.add(CopyAction).setName("Copy");
        editMenu.add(PasteAction).setName("Paste");
        jMenuBar.add(editMenu);

        SaveAction.setEnabled(anyChanges);
        SaveAsAction.setEnabled(anyChanges);

        jTextArea.addKeyListener(textBoxKeyListener);
        setTitle("Untitled.txt");
        pack();
        setVisible(true);
    }

    private void saveFileAs() {
        if (path != null) jFileChooser.setCurrentDirectory(path.toFile());
        if(jFileChooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
            path = Paths.get(jFileChooser.getSelectedFile().getAbsolutePath());
            writeFile(path);
        }
    }

    private void readFile(Path filePath) {
        try {
            FileReader reader = new FileReader(filePath.toAbsolutePath().toString());
            jTextArea.read(reader,null);
            reader.close();
            anyChanges = false;
            setTitle(filePath.getFileName().toString());
        }
        catch(IOException e) {
            JOptionPane.showMessageDialog(this,"Can't read file: "+filePath.toAbsolutePath().toString() + " error: " + e.getLocalizedMessage());
        }
    }

    private void writeFile(Path filePath) {
        try {
            FileWriter writer = new FileWriter(filePath.toAbsolutePath().toString());
            jTextArea.write(writer);
            writer.close();
            setTitle(filePath.getFileName().toString());
            anyChanges = false;
            SaveAction.setEnabled(false);
        }
        catch(IOException e) {
            JOptionPane.showMessageDialog(this,"Can't write file: "+filePath.toAbsolutePath().toString() + "error: " + e.getLocalizedMessage());
        }
    }

    private void saveCurrent() {
        if(anyChanges) {
            if(JOptionPane.showConfirmDialog(this, "Would you like to save ?","SaveAction",
                    JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION)
                writeFile(path);
        }
    }

    private KeyListener textBoxKeyListener = new KeyAdapter() {
        public void keyPressed(KeyEvent e) {
            anyChanges = true;
            SaveAction.setEnabled(true);
            SaveAsAction.setEnabled(true);
        }
    };


}

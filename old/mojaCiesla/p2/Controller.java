package mojaCiesla.p2;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.Inet4Address;

public class Controller implements TicTacUI {

    public Button button1;
    public Button button2;
    public Button button3;
    public Button button4;
    public Button button5;
    public Button button6;
    public Button button7;
    public Button button8;
    public Button button9;
    public Button buttonStart;
    public CheckBox clientModeCheckBox;
    public TextField fieldIP;
    private boolean isServerMode = true;

    TicTacNetworkInterface ticTacNetworkInterface;

    @FXML
    public void initialize() {
        setButtonStatusDisable(true);
    }

    @Override
    public void updateMap() {
        Platform.runLater(() -> {
            System.out.println("Updating");
            button1.setText(FieldStatusToString(ticTacNetworkInterface.getField(0, 0)));
            button2.setText(FieldStatusToString(ticTacNetworkInterface.getField(0, 1)));
            button3.setText(FieldStatusToString(ticTacNetworkInterface.getField(0, 2)));
            button4.setText(FieldStatusToString(ticTacNetworkInterface.getField(1, 0)));
            button5.setText(FieldStatusToString(ticTacNetworkInterface.getField(1, 1)));
            button6.setText(FieldStatusToString(ticTacNetworkInterface.getField(1, 2)));
            button7.setText(FieldStatusToString(ticTacNetworkInterface.getField(2, 0)));
            button8.setText(FieldStatusToString(ticTacNetworkInterface.getField(2, 1)));
            button9.setText(FieldStatusToString(ticTacNetworkInterface.getField(2, 2)));
        });
    }

    @Override
    public void readyForPlay() {
        Platform.runLater(() -> {
        setButtonStatusDisable(false);
        });
    }

    @Override
    public void blockPlay() {
        Platform.runLater(() -> {
            setButtonStatusDisable(true);
        });
    }

    private void setButtonStatusDisable(Boolean status){
        button1.setDisable(status);
        button2.setDisable(status);
        button3.setDisable(status);
        button4.setDisable(status);
        button5.setDisable(status);
        button6.setDisable(status);
        button7.setDisable(status);
        button8.setDisable(status);
        button9.setDisable(status);
    }

    @Override
    public void showMessage(String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(message);
           // alert.setContentText("I have a great message for you!");

            alert.showAndWait();
        });

    }

    private String FieldStatusToString(Field f){
        switch (f){
            case Empty:
                return "";
            case Crosses:
                return "X";
            case Noughts:
                return "O";
        }
        return "?";
    }

    public void OnButton1Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(0,0);
    }

    public void OnButton2Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(0,1);
    }

    public void OnButton3Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(0,2);
    }

    public void OnButton4Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(1,0);
    }

    public void OnButton5Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(1,1);
    }

    public void OnButton6Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(1,2);
    }

    public void OnButton7Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(2,0);
    }

    public void OnButton8Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(2,1);
    }

    public void OnButton9Click(ActionEvent actionEvent) {
        ticTacNetworkInterface.Select(2,2);
    }

    public void modeChanged(ActionEvent actionEvent) {
       if (clientModeCheckBox.isSelected()) isServerMode = false;
       else isServerMode = true;
    }

    public void onStartClick(ActionEvent actionEvent) {
        try {
            if (isServerMode == false) ticTacNetworkInterface = new TicTacClient(Inet4Address.getByName(fieldIP.getText()), 8976);
            else ticTacNetworkInterface = new TicTacServer(8976);

            buttonStart.setDisable(true);
            ticTacNetworkInterface.attachUI(this);
            ticTacNetworkInterface.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void OnLoadButtonClick(ActionEvent actionEvent) throws JAXBException{
        TicTacServer ticTacServer = (TicTacServer) ticTacNetworkInterface;
        ticTacServer.readFromFile();
    }

    public void OnSaveButtonClick(ActionEvent actionEvent) throws JAXBException {
        TicTacServer ticTacServer = (TicTacServer) ticTacNetworkInterface;
        ticTacServer.saveToFile();
    }
}

package mojaCiesla.p3;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.Inet4Address;
import java.util.HashMap;
import java.util.Map;

public class Controller implements TicTacUI {

    public GridPane GameField;
    public Button buttonStart;
    public CheckBox clientModeCheckBox;
    public TextField fieldIP;
    public TextField fieldHistoryName;
    public Button LoadButton;
    private boolean isServerMode = true;
    private Map<Pair<Integer, Integer>, Button> buttonMap = new HashMap<>();
    TicTacNetworkInterface ticTacNetworkInterface;

    @Override
    public void updateMap() {
        Platform.runLater(() -> {
            System.out.println("Updating");
            buttonMap.forEach((k,v) -> {
                String text = FieldStatusToString(ticTacNetworkInterface.getField(k.getKey(), k.getValue()));
                if (ticTacNetworkInterface.getField(k.getKey(), k.getValue()) != Field.Empty) v.setDisable(true);
                v.setText(text);
            });
        });
    }

    @Override
    public void readyForPlay() {
        Platform.runLater(() -> {
            buttonMap.forEach((k,v) -> {
                if (ticTacNetworkInterface.getField(k.getKey(), k.getValue()) == Field.Empty) v.setDisable(false);
            });
        });
    }

    @Override
    public void blockPlay() {
        Platform.runLater(() -> {
            setButtonStatus(true);
        });
    }

    private void setButtonStatus(Boolean status){
        buttonMap.forEach((k,v) -> v.setDisable(status));
    }

    @Override
    public void showMessage(String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(message);
            alert.showAndWait();
        });

    }

    private String FieldStatusToString(Field f){
        switch (f){
            case Empty:
                return "";
            case Crosses:
                return "X";
            case Noughts:
                return "O";
        }
        return "?";
    }

    public void modeChanged(ActionEvent actionEvent) {
       if (clientModeCheckBox.isSelected()) isServerMode = false;
       else isServerMode = true;
    }

    public void onStartClick(ActionEvent actionEvent) {
        Platform.runLater(() -> {
            fieldHistoryName.setDisable(true);
            LoadButton.setDisable(true);
            initGameField();
        });
        try {
            if (isServerMode == false) ticTacNetworkInterface = new TicTacClient(Inet4Address.getByName(fieldIP.getText()), 8976);
            else ticTacNetworkInterface = new TicTacServer(8976);

            buttonStart.setDisable(true);

            ticTacNetworkInterface.attachUI(this);
            ticTacNetworkInterface.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initGameField(){
        for(int y = 0; ticTacNetworkInterface.getSize() > y; y++){
            for(int x = 0; ticTacNetworkInterface.getSize() > x; x++){
                Button button = new Button();
                button.setDisable(true);
                button.setMaxSize(30,30);
                button.setMinSize(30,30);
                final int x_f = x;
                final int y_f = y;
                buttonMap.put(new Pair<>(x,y), button);
                button.setOnAction((event) -> Platform.runLater(() -> {
                    System.out.println("Click:" + x_f + "," + y_f);
                    this.ticTacNetworkInterface.Select(x_f,y_f);
                }));
                GameField.add(button, x, y);
            }

        }

    }

    public void OnLoadHistory(ActionEvent actionEvent) {
        Thread thread = new Thread(() -> {
            GameHistory gameHistory = new GameHistory();
            ticTacNetworkInterface = gameHistory;
            initGameField();
            gameHistory.loadGame(fieldHistoryName.getText(), this);
        });
        thread.run();
    }
}

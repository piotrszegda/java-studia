package mojaCiesla.p2;

import javafx.util.Pair;

enum ClientToServerRequestType{
    SendOwnMove
}

public class ClientToServerRequest implements java.io.Serializable{
    public ClientToServerRequestType clientToServerRequestType;
    Pair<Integer, Integer> move;

}

package com.munk.wyklad4;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class OwnPriorityQueue<T> {
    private final AtomicInteger mySize = new AtomicInteger();
    private final AtomicReference<ArrayList<Pair<T, Integer>>> myList = new AtomicReference<ArrayList<Pair<T, Integer>>>();

    public OwnPriorityQueue()
    {
        myList.set(new ArrayList<>(32));
        myList.get().add(null);
        mySize.set(0);
    }



    public boolean add(T t, int priotity)
    {
        var  o = new Pair<>(t, priotity);
        myList.get().add(o);
        mySize.getAndIncrement();
        int k = mySize.get();

        while (k > 1 && myList.get().get(k / 2).getValue() > priotity)
        {
            myList.get().set(k, myList.get().get(k/2));
            k /= 2;
        }
        myList.get().set(k,o);

        return true;
    }


    public int size()
    {
        return mySize.get();
    }


    public boolean isEmpty()
    {
        return mySize.get() == 0;
    }

    public T get()
    {
        if (isEmpty()) {
            return null;
        }
        var hold = myList.get().get(1);

        myList.get().set(1, myList.get().get(mySize.get()));
        myList.get().remove(mySize.get());
        mySize.getAndDecrement();
        if (mySize.get() > 1)
        {
            heapify(1);
        }
        return hold.getKey();
    }

    public Object peek()
    {
        return myList.get().get(1);
    }

    private void heapify(int vroot)
    {
        var last = myList.get().get(vroot);
        int child, k = vroot;
        while (2*k <= mySize.get())
        {
            child = 2*k;
            if (child < mySize.get() &&
                    myList.get().get(child).getValue() > myList.get().get(child + 1).getValue())
            {
                child++;
            }
            if (last.getValue() <= myList.get().get(child).getValue())
            {
                break;
            }
            else
            {
                myList.get().set(k, myList.get().get(child));
                k = child;
            }
        }
        myList.get().set(k, last);
    }


}

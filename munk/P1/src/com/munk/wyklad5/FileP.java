package com.munk.wyklad5;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

public class FileP {
    private BufferedReader in;
    private Path path;
    public FileP(Path path){
        try {
            in = new BufferedReader(new FileReader(path.toString()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        this.path = path;

    }
    public void start(){
        ConcurrentMap<String, AtomicLong> map = new ConcurrentHashMap<>();

        var s = new Scanner(in);
        while (s.hasNext()) {
            var world = s.next();
            for (var ch: world.toCharArray()) {
                var st = String.valueOf(ch);
                map.putIfAbsent(st, new AtomicLong(0));
                map.get(st).incrementAndGet();
            }
            map.putIfAbsent(world, new AtomicLong(0));
            map.get(world).incrementAndGet();
        }



        var cmpr = new Comparator<Pair<String,AtomicLong>>() {

            @Override
            public int compare(Pair<String, AtomicLong> o1, Pair<String, AtomicLong> o2) {
                return Long.compare(o1.getValue().get(), o2.getValue().get());
            }
        };

        var words = new PriorityQueue<>(cmpr);
        var characters = new PriorityQueue<>(cmpr);

        for (var ch: map.entrySet()) {
            if (ch.getKey().length() == 1){
                characters.add(new Pair(ch.getKey(), ch.getValue()));
            }
            else{
                words.add(new Pair(ch.getKey(), ch.getValue()));
            }

        }
        System.out.println("Litery:");
        while(!characters.isEmpty()){
            var w = characters.peek();
            System.out.println(w);
            characters.remove(w);
        }
        System.out.println("Słowa:");
        while(!words.isEmpty()){
            var w = words.peek();
            System.out.println(w);
            words.remove(w);
        }

        try {
            in = new BufferedReader(new FileReader(path.toString()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Linie:" +  in.lines().count());

        int n = 0;

        try {
            n = new String (Files.readAllBytes(path)).split ("[\\.\\?!]").length;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Zdania:" +  n);


    }
}

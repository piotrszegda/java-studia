package com.munk.wyklad6;

import javafx.util.Pair;

import java.util.concurrent.atomic.AtomicLong;

public class SimplePrime {
    public static Pair<Boolean, Long> calc(Long input){
        Long current = input - 1;

        while(current > 1){
            if (input % current == 0){
                return new Pair<>(false, input);
            }
            current--;
        }
        return new Pair<>(true, input);

    }
}

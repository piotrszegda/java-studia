package com.munk.wyklad6;

import javafx.util.Pair;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class SimplePrimeAsync {
    public static Future<Pair<Boolean, Long>> calculateAsync(Long input) throws InterruptedException {
        CompletableFuture<Pair<Boolean, Long>> completableFuture
                = new CompletableFuture<>();

        Executors.newCachedThreadPool().submit(() -> {
            if (input == 1) completableFuture.complete(new Pair(true, input));

            long current = input- 1;
            while(current > 1){
                if (input % current == 0){
                    completableFuture.complete(new Pair(false, input));
                }
                current--;
            }
            completableFuture.complete(new Pair(true, input));
            return null;
        });

        return completableFuture;
    }

}
